# Tensorflow Viewing Party Demo
A quick demo on how to use TensorFlow Estimator as well as Google Cloud ML Engine.

### Training and Evaluating
To train and evaluate models, simply run:

    gcloud ml-engine jobs submit training $JOB_NAME \
	--job-dir $JOB_DIR \
	--runtime-version 1.10 \
	--module-name trainer.main \
	--packages packages/cython-0.28.5.tar.gz,packages/tensornets-0.3.5.tar.gz \
	--package-path trainer \
	--region asia-east1 \
	--config config.yaml \
	-- \
	--train-dir 101_ObjectCategories \
	--training-steps 1 \
	--export-dir gs://experiment-203002-mlengine/export/ \
	--load-pretrained True
	
For more information you can read [this tutorial](https://cloud.google.com/ml-engine/docs/tensorflow/getting-started-training-prediction) for better
understanding on how to use Google Cloud ML Engine.

### Serving model
You can host your exported model on Google Cloud ML
Engine as well as TensorFlow Serving. On how to deploy on Google Cloud ML Engine you can follow [this 
link](https://cloud.google.com/ml-engine/docs/tensorflow/getting-started-training-prediction#deploy_a_model_to_support_prediction),
for TensorFlow Serving using [this link](https://www.tensorflow.org/tfx/serving/docker)

Send example request to TensorFlow Serving through gRPC client using the following command:

    python grpc_client.py --model test --image <path to image> --signature-name serving_default