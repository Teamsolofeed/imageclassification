from __future__ import print_function

import argparse
import time
import numpy as np
from skimage import io, transform
import grpc
from tensorflow.contrib.util import make_tensor_proto
from tensorflow_serving.apis import predict_pb2
from tensorflow_serving.apis import prediction_service_pb2_grpc
from trainer.config import *


def run(host, port, image, model, signature_name):

    channel = grpc.insecure_channel('%s:%d' % (host, port))
    stub = prediction_service_pb2_grpc.PredictionServiceStub(channel)

    # Read an image
    image = io.imread(image)
    data = transform.resize(image, [IMAGE_SIZE, IMAGE_SIZE, IMAGE_DEPTH], mode='constant')
    data = data.astype(np.float32)

    start = time.time()

    # Call classification model to make prediction on the image
    request = predict_pb2.PredictRequest()
    request.model_spec.name = model
    request.model_spec.signature_name = signature_name
    request.inputs['images'].CopyFrom(make_tensor_proto(data, shape=[1, IMAGE_SIZE, IMAGE_SIZE, IMAGE_DEPTH]))

    result = stub.Predict(request, 10.0)

    end = time.time()
    time_diff = end - start

    # Reference:
    # How to access nested values
    # https://stackoverflow.com/questions/44785847/how-to-retrieve-float-val-from-a-predictresponse-object
    print(result)
    print('time elapased: {}'.format(time_diff))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--host', help='Tensorflow server host name', default='localhost', type=str)
    parser.add_argument('--port', help='Tensorflow server port number', default=8500, type=int)
    parser.add_argument('--image', help='input image', type=str)
    parser.add_argument('--model', help='model name', default='test')
    parser.add_argument('--signature-name', help='Signature name of saved TF model',
                        default='serving_default', type=str)

    args = parser.parse_args()
    run(args.host, args.port, args.image, args.model, args.signature_name)
