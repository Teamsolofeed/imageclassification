from setuptools import find_packages
from setuptools import setup

REQUIRED_PACKAGES = ['h5py', 'scikit-image==0.14.0', 'google-cloud<0.27.0', 'google-cloud-storage<1.3.0']

setup(
    name='trainer',
    version='0.1',
    install_requires=REQUIRED_PACKAGES,
    packages=find_packages(),
    include_package_data=True,
    description='My training application package.'
)