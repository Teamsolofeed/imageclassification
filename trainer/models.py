import tensornets as nets
import tensorflow as tf
import logging
from config import *

LOGGER = logging.getLogger(__name__)


def Inception3(features, labels, mode, params):
    """
    Pretrained Inception3
    :param features: Tensor, training data in tensor
    :param labels: Tensor
    :param mode: String tf.estimator.ModeKeys
    :param params: Dictionaries, additional parameters for the model
    :return: tf.estimator.EstiamtorSpec
    """
    model = nets.Inception3(features['images'], is_training=True, classes=params["n_classes"])

    # Load the pre-trained weight at the start of the training step 0
    if params["load_pretrained"]:
        with tf.Session() as sess:
            LOGGER.info("Load pretrained model")
            sess.run(model.pretrained())

    W = tf.Variable(tf.truncated_normal(
        [2048, params["n_classes"]], stddev=0.1))
    b = tf.Variable(tf.zeros(params["n_classes"]))

    outputs = model.get_outputs()

    # Get the last fully connected layer and calculate the new logits
    # since we try to predict on a different number of labels
    logits = tf.matmul(outputs[-3], W) + b

    # Compute prediction
    predicted_classes = tf.argmax(logits, 1)

    # PREDICT mode
    if mode == tf.estimator.ModeKeys.PREDICT:
        predictions = {
            "classes": predicted_classes,
            "probabilities": tf.nn.softmax(logits)
        }
        return tf.estimator.EstimatorSpec(
            mode=mode,
            predictions=predictions,

        )

    labels_one_hot = tf.one_hot(
        labels,
        depth=params["n_classes"],
        on_value=True,
        off_value=False,
        dtype=tf.bool)

    # Compute loss
    loss = tf.losses.softmax_cross_entropy(labels_one_hot, logits=logits)

    # Compute evaluation metrics
    accuracy = tf.metrics.accuracy(labels=labels,
                                   predictions=predicted_classes,
                                   name="acc_op")
    metrics = {"acc_eval": accuracy}

    # For displaying on TensorBoard
    tf.summary.scalar("accuracy", accuracy[1])

    if mode == tf.estimator.ModeKeys.EVAL:
        return tf.estimator.EstimatorSpec(
            mode, loss=loss, eval_metric_ops=metrics)

    # Create training op
    assert mode == tf.estimator.ModeKeys.TRAIN, "Unknown mode"

    # Decay every 1000 steps with a base of 0.96
    global_step = tf.train.get_global_step()
    learning_rate = tf.train.exponential_decay(params["learning_rate"], global_step=global_step,
                                               decay_steps=1000, decay_rate=0.96, staircase=True)
    optimizer = tf.train.AdagradOptimizer(learning_rate=learning_rate)
    train_op = optimizer.minimize(loss, global_step=global_step)

    return tf.estimator.EstimatorSpec(mode, loss=loss, train_op=train_op)


def serving_input_receiver_fn():
    """
    This is used to define inputs to serve the model
    :return: ServingInputReceiver
    """
    receiver_tensors = {
        # The size of input image is flexible
        'images': tf.placeholder(tf.float32, [None, None, None, IMAGE_DEPTH])
    }

    # Convert given inputs to feed to the model
    features = {
        'images': tf.image.resize_images(receiver_tensors['images'], [IMAGE_SIZE, IMAGE_SIZE])
    }
    return tf.estimator.export.ServingInputReceiver(receiver_tensors=receiver_tensors,
                                                    features=features)

