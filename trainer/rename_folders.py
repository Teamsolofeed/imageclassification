import os
import pandas as pd


def rename_folder(dir):
    label_int = []
    label_str = []
    print("Rename the folder")
    for index, label in enumerate(os.listdir(dir)):
        label_int.append(index)
        label_str.append(label)
        os.rename(os.path.join(dir, label), os.path.join(dir, str(index)))

    print("Save map label")
    df = pd.DataFrame({"id": label_int, "value": label_str})
    df.to_csv("../label_id.csv", index=False)


rename_folder('../101_ObjectCategories')
