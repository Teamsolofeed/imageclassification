import tensorflow as tf
from sklearn.model_selection import train_test_split
import os
from skimage import io, transform
import numpy as np
from google.cloud import storage
import logging
from StringIO import StringIO
from config import *

LOGGER = logging.getLogger(__name__)

client = storage.Client()
bucket = client.bucket(BUCKET_NAME)


def load_data_train(train_dir, val_ratio=0.1):
    """
    Load train data and evaluation data
    :param train_dir: String, name of training directory
    :param val_ratio: Float, fraction of data for validation
    :return: (List, List), (List, List)
    """
    img_arr = []
    label_arr = []

    LOGGER.info("Begin reading data from file")
    for label in os.listdir(train_dir):
        img_class = os.path.join(train_dir, label)
        if not os.path.isdir(img_class):
            continue
        for img in os.listdir(img_class):
            if "jpg" not in img:
                continue
            img_arr.append(os.path.join(train_dir, label, img))
            label_arr.append(int(label))

    LOGGER.info("Shuffling data and splitting data")

    X_train, X_test, y_train, y_test = train_test_split(img_arr, label_arr, shuffle=True,
                                                        test_size=val_ratio)

    return (X_train, y_train), (X_test, y_test)


def load_data_train_gcs(train_dir, val_ratio=0.1):
    """
        Load train data and evaluation data on GCS
        :param train_dir: String, name of training directory
        :param val_ratio: Float, fraction of data for validation
        :return: (List, List), (List, List)
    """
    img_arr = []
    label_arr = []
    LOGGER.info("Begin reading data from file")
    for blob in bucket.list_blobs(prefix=train_dir):
        try:
            if '.DS_Store' in blob.name:
                continue
            tmp = blob.name.split('/')
            img_arr.append(blob.name)
            label_arr.append(int(tmp[-2]))
        except Exception as e:
            LOGGER.warn(e)

    LOGGER.info("Shuffling data and splitting data")

    X_train, X_test, y_train, y_test = train_test_split(img_arr, label_arr, shuffle=True,
                                                        test_size=val_ratio)

    return (X_train, y_train), (X_test, y_test)


def _read_py_function(filename, label):
    # Download from GCS
    blob = bucket.blob(filename)
    image = blob.download_as_string()
    image = io.imread(StringIO(image))
    image = transform.resize(image, [IMAGE_SIZE, IMAGE_SIZE, IMAGE_DEPTH], mode="constant")
    return image.astype(np.float32), label


def _resize_tensor(image, label):
    image.set_shape([IMAGE_SIZE, IMAGE_SIZE, IMAGE_DEPTH])
    return image, label


def input_fn(images, labels, batch_size, num_epoch):
    """
    An input function for training and evaluating
    :param images: List, list of training image names
    :param labels: List, list of training labels
    :param batch_size: Int, training batch data size
    :param num_epoch: Int, number of epoch to train
    :return: tf.data.Dataset
    """

    # Convert the inputs to Dataset
    dataset = tf.data.Dataset.from_tensor_slices((images, labels))

    # Parse to image
    dataset = dataset.map(
        lambda filename, label: tuple(tf.py_func(
            _read_py_function, [filename, label], [tf.float32, label.dtype])))

    dataset = dataset.map(_resize_tensor)

    # repeat and batch
    dataset = dataset.repeat(count=num_epoch).batch(batch_size)

    features, labels = dataset.make_one_shot_iterator().get_next()

    return {"images": features}, labels
