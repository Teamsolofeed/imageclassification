import argparse
from load_dataset import *
from models import *

logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)s: %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
LOGGER = logging.getLogger(__name__)


def run_model(args):
    """
    Load data, training model and export model
    :param args: Argparse object, contains list of command argument paramater
    :return:
    """
    # Load data
    LOGGER.info("Read data")
    (train_x, train_y), (eval_x, eval_y) = load_data_train_gcs(args.train_dir, val_ratio=0.1)

    # Create the Estimator
    LOGGER.info("Config training")
    training_config = tf.estimator.RunConfig(
        model_dir=args.job_dir,
        save_summary_steps=50,
        save_checkpoints_steps=100,
        keep_checkpoint_max=2)

    params = {
        "learning_rate": args.learning_rate,
        "n_classes": args.num_classes
    }

    if args.load_pretrained == 'True':
        params["load_pretrained"] = True
    else:
        params["load_pretrained"] = False

    classifier = tf.estimator.Estimator(
        model_fn=Inception3,
        params=params,
        config=training_config)

    # Train the model
    classifier.train(
        input_fn=lambda: input_fn(train_x, train_y, args.batch_size, args.epochs),
        max_steps=args.training_steps
    )

    # Do not evaluate and export model when load_pretrained is True
    # Because it will load the pretrained model again not our current trained model
    if args.load_pretrained == 'True':
        return

    # Evaluate the model and print results
    eval_result = classifier.evaluate(
        input_fn=lambda: input_fn(eval_x, eval_y, batch_size=args.batch_size, num_epoch=1), steps=None)
    LOGGER.info(eval_result)

    # Export model
    LOGGER.info("Export model")
    classifier.export_savedmodel(args.export_dir, serving_input_receiver_fn=serving_input_receiver_fn)


def main(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument("--train-dir", help="File directory to train", required=True)
    parser.add_argument("--batch-size", default=50, help="Batch size", type=int)
    parser.add_argument("--training-steps", default=None, help="Number of training steps", type=int)
    parser.add_argument("--learning-rate", default=0.1, help="Learning rate", type=float)
    parser.add_argument("--epochs", default=1, help="Number of epoch to train", type=int)
    parser.add_argument("--job-dir", required=True, help="Directory to save model", type=str)
    parser.add_argument("--export-dir", required=True, help="Directory to export model for serving",
                        type=str)
    parser.add_argument("--load-pretrained", choices=['True', 'False'],
                        help="Whether or not loading the pretrained model")
    parser.add_argument("--verbosity",
                        choices=[
                            'DEBUG',
                            'ERROR',
                            'FATAL',
                            'INFO',
                            'WARN'
                        ], default="INFO", help="Set logging verbosity")
    parser.add_argument("--num_classes", default=102, help="Number of classes", type=int)

    args = parser.parse_args(argv[1:])

    # Set python level verbosity
    tf.logging.set_verbosity(args.verbosity)

    # Set C++ Graph Excecution level verbosity
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = str(
        tf.logging.__dict__[args.verbosity] / 10
    )

    # Run the training job
    run_model(args)


if __name__ == "__main__":
    tf.app.run(main)
